#!/usr/bin/env python3

import re
from pathlib import Path

error_file = Path("win32error.txt").open()
codes = error_file.read().split('\n')

code_dict = {}
i = 0

p = re.compile('(\d+).+')

print('$errorCodes = @{}')

for line in codes:
  index = p.match(line)
  if index != None:
    print('$errorCodes[' + index.group(1) + '] = "' + codes[i - 1] + " " + codes[i + 1] + '"')

  i += 1
